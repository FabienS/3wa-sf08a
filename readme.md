# Installer le projet

> Il est possible de cloner ce projet sur l'IDE ou en local chez vous

## Prérequis :

- php 8
- composer
- npm et/ou yarn
- mysql 5.7
- l'exécutable symfony pour un serveur en local

## Clonez le dépôt

```bash
# Créez un dossier
mkdir projetEcommerce

# Déplacez vous dans ce dossier 
cd projetEcommerce/

# Clonez ce dépôt à la racine de ce répertoire
git clone https://gitlab.com/FabienS/3wa-sf08a.git ./

```


## Composer / Npm

```bash

# Installer les dépendances Symfony

composer install

# Installer les dépendances Encore Webpack
npm install

```

## Gestion des assets

Il faut modifier le dossier des assets en local dans le fichier webpack.config.js (racine) :

```js
// ./webpack.config.js
// pour une installation sur l'ide ( projetEcommerce est votre dossier créé au début)
.setPublicPath('/projetEcommerce/public/build')

// pour une utilisation avec le serveur symfony
.setPublicPath('/build')

```

```bash

# Compiler les assets
npm run dev

```


## Configuration

- copier le fichier .env vers .env.local

- Dans le .env.local modifier la variable d'environnement DATABASE_URL avec vos informations

## Database

```bash
# Création avec les données DATABASE_URL
php bin/console doctrine:database:create


# migration
php bin/console doctrine:migrations:migrate

```


## Tester 


### Sur l'IDE :

- Connecter à l'URL en rajoutant public/
- Créez un utilisateur 
- Ajouter le role ROLE_ADMIN dans la base de données pour cet utilisateur
- Se connecter en allant sur public/admin/product/
- Ajouter des catéforie, une tva et des produits !
- Tester le front en allant sur public/


### En local avec un dossier dans WAMP/MAMP...

- faire la même choses que ci-dessus mais à partir de l'URL : http://localhost/projetEcommerce/public/


### En local avec le serveur Symfony (méthode conseillée)

```bash
## supprimer le apache-pack
composer remove symfony/apache-pack

# a la racine de votre dossier clone, lancez le serveur Symfony
symfony serve

# suivez l'url proposé : exemple : 127.0.0.1:8000

```

# Vous rencontrez des problèmes :

- Contactez moi sur le serveur Discord
- Ou laissez une issue ici en cas de problème 