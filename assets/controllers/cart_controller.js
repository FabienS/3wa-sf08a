import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    static targets = ["qte"]
    static values = { url: String }
    
    sendData(e) {
        e.preventDefault();
        
        alert(this.urlValue);
        
        fetch(this.urlValue, {
            method: 'post', 
            body: JSON.stringify({ qte: this.qteTarget }
            )})
            .then(response => response.text())
            .then(html => alert(html))
    }
}
