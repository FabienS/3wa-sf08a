<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Tva;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('quantity')
            ->add('reference')
            ->add('weight')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'title'
                ]
            )
            ->add('valid')
            ->add('tva', EntityType::class, [
                'class' => Tva::class,
                'choice_label' => 'name'
                ]
            );
            
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
