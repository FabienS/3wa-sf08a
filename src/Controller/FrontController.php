<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontController extends AbstractController
{
    
    
    #[Route('/', name: 'home')]
    public function indexNoLocale(): Response
    {
        return $this->redirectToRoute('homepage', ['_locale' => 'fr']);
    }

    #[Route('/{_locale<%app.supported_locales%>}', name: 'homepage')]
    public function index(ProductRepository $productRepository): Response
    {
        
        
        return $this->render('front/index.html.twig', [
            'products' => $productRepository->findBy(['valid'=>true],['createdAt'=>'DESC'],10,0),
        ]);
    }
    
     #[Route('/{_locale<%app.supported_locales%>}/product/{slug}', name: 'product_detail')]
    public function productDetail(Product $product): Response
    {
        
        
        return $this->render('front/productDetail.html.twig', [
           'product'=> $product
        ]);
    }
    
    #[Route('/{_locale<%app.supported_locales%>}/pages/{page}', name: 'static')]
    public function static(string $_locale, string $page=null, Environment $twig): Response
    {

        $loader = $twig->getLoader();
        if (!$loader->exists("front/pages/$page.$_locale.html.twig"))
            throw new NotFoundHttpException();

        return $this->render("front/pages/$page.$_locale.html.twig");
    }
    

    #[Route('/{_locale<%app.supported_locales%>}/category/{slug}', name: 'category')]
    public function category(Category $category): Response
    {
        return $this->render('com/category.html.twig', ['category' => $category]);
    }
    
    
    

    #[Route('/{_locale<%app.supported_locales%>}/categories', name: 'categories')]
    public function categories(CategoryRepository $categoryRepository): Response
    {
        return $this->render('com/categories.html.twig', ['categories' => $categoryRepository->findBy([],['title'=>'ASC'])]);
    }
    
    
    /** Retourne une navigation des catégories
     * Cette méthode est appelée dans la vue base.html.twig pour générer le menu des catégories
     */
    public function categoriesNavigation(?int $parent = null, CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findBy(['parent' => $parent], $orderBy = ['title' => 'ASC']);

        return $this->render('front/_categoriesNavigation.html.twig', ['categories' => $categories]);
    }
}
