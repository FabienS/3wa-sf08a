<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

#[Route('/cart')]
class CartController extends AbstractController
{
    #[Route('/', name: 'cart')]
    public function index(): Response
    {
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }
    
    #[Route('/add/{id}', name: 'cart_add', methods: ['POST'])]
    public function add(Product $product, SessionInterface $session, Request $request, ProductRepository $productRepository): Response
    {
        $qte = $request->request->get('quantity', 0);
        
        $session->set('cart', []);
        
        $cart = $session->get('cart', []);
        
        if(empty($cart)) {
            $cart['products']   = [];
            $cart['values']     = [
                'totalQte'  =>0,
                'total'     =>0,
                'totalTva'  =>0,
                'totalTTC'  =>0
                ];
        }
        
        $productId = $product->getId();
        
        if(isset($cart['products'][$productId])) {
            $cart['products'][$productId]['qte'] += $qte;
        }
        else {
            $tva = ($product->getPrice() * $product->getTva()->getRate() /100);

            $cart['products'][$productId] = [
                'qte'       => $qte,
                'name'      => $product->getName(),
                'slug'      => $product->getSlug(),
                'price'     => $product->getPrice(),
                'tva'       => $tva,
                'priceTTC'  => $tva + $product->getPrice(),
            ];
            
            if(!$product->getProductPictures()->isEmpty()) {
               $cart['products'][$productId]['picture'] = $product->getProductPictures()[0]->getFile();
            }
        }
        
        $cart['products'][$productId]['totalTTC'] = $cart['products'][$productId]['qte'] * $cart['products'][$productId]['priceTTC'];
        
        $cart['values']['totalQte'] += $qte;
        $cart['values']['total']    += $cart['products'][$productId]['price'];
        $cart['values']['totalTva'] += $cart['products'][$productId]['tva'];
        $cart['values']['totalTTC'] += $cart['products'][$productId]['totalTTC'];
        
        
        $session->set('cart', $cart);
        
        if ($request->isXmlHttpRequest()) {
            return $this->json($cart['values']);
        }
        
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }
}
