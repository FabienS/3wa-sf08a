<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductPicture;
use App\Form\ProductType;
use App\Form\ProductPictureType;
use App\Service\FileUploader;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/product')]
class AdminProductController extends AbstractController
{
    #[Route('/', name: 'admin_product_index', methods: ['GET'])]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('admin_product/index.html.twig', [
            'products' => $productRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_product_edit', methods: ['GET', 'POST'])]
    #[Route('/new', name: 'admin_product_new', methods: ['GET', 'POST'])]
    public function new(Product $product=null, Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response
    {
        if($product == null)
            $product = new Product();
            
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            if($product->getId()==null) 
                $product->setCreatedAt(new \DateTimeImmutable());
                
            $product->setSlug($slugger->slug(strtolower($product->getName())));
            
            $entityManager->persist($product);
            $entityManager->flush();
            
            $this->addFlash('success','Le produit a bien été créé !');

            return $this->redirectToRoute('admin_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_product/new.html.twig', [
            'product' => $product,
            'form' => $form,
            'edit' => ($product->getId()==null)?false:true
        ]);
    }
    
    #[Route('/{id}/pictures', name: 'admin_product_picture_index', methods: ['GET'])]
    public function indexPictures(Product $product): Response
    {
        return $this->render('admin_product/indexPictures.html.twig', [
            'productName' => $product->getName(),
            'pictures' => $product->getProductPictures(),
            'productId'=> $product->getId()
        ]);
    }
    
    #[Route('/{id}/pictures/new', name: 'admin_product_picture_new', methods: ['GET', 'POST'])]
    public function newPicture(Product $product, Request $request, EntityManagerInterface $entityManager, FileUploader $fileUploader): Response
    {
        $picture = new ProductPicture();
        $picture->setProduct($product);
        
        $form = $this->createForm(ProductPictureType::class, $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            if($picture->getId()==null) 
                $picture->setCreatedAt(new \DateTimeImmutable());
                
            $pictureFile = $form->get('file')->getData();
            if ($pictureFile) {
                $pictureName = $fileUploader->upload($pictureFile, 'product');
                
                // Suppression de l'image si elle existe déjà
                $fileUploader->remove($picture->getFile(),'product');
                
                $picture->setFile($pictureName);
            }
            
            $entityManager->persist($picture);
            $entityManager->flush();

            return $this->redirectToRoute('admin_product_picture_index', ['id'=>$product->getId()], Response::HTTP_SEE_OTHER);
        }
        
        return $this->renderForm('admin_product/newPicture.html.twig', [
            'form' => $form,
            'productName' => $product->getName(),
            'productId'=> $product->getId()
        ]);
    }


    #[Route('/{id}', name: 'admin_product_show', methods: ['GET'])]
    public function show(Product $product): Response
    {
        return $this->render('admin_product/show.html.twig', [
            'product' => $product,
        ]);
    }


    #[Route('/{id}', name: 'admin_product_delete', methods: ['POST'])]
    public function delete(Request $request, Product $product, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_product_index', [], Response::HTTP_SEE_OTHER);
    }
}
