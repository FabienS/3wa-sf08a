# Site Ecommerce 


Commencez un nouvelle installation propre de Symfony

## Installer et configurer : 

- l'accès à la base de données (.env.local) + création de la base
- les fixtures et faker (composer)
- l'utilisation de bootstrap et stimulus avec le Webpack Encore (inclure boostrap dans notre modèle de base HTML)
- si l'accès à Symfony est trop lent essayer de désactiver le cache comme fait précédemment
- installer Apache-pack avec composer,
- préparer un controller `FrontController` et une route/méthode pour la page d'accueil
- dans l'HTML de base intégrer une NavBar avec :
  - Un lien vers la home,
  - Un menu déroulant `Catégories` qui ne pointe vers rien pour le moment
  - Un menu `Contact` qui ne pointe vers rien vers rien pour le moment
  - Un menu `A propos` qui ne pointe vers rien pour le moment
  - Aligner à droite un menu déroulant `Panier` avec un icône de panier devant (voir ci-après pour les icônes) et une zone pour le nombre d'article dedans... la partie déroulante contiendra les éléments du panier plus tard.
- pour les icônes utilisez `Icomoon App` disponible en ligne qui permet de sélectionner les icônes dont nous avons besoin uniquement et mettre le résultat des icônes générées dans les assets de Webpack... ajouter le css à votre point d'entrée
- dans le pied de page mettre un menu avec
  - Mentions légales - qui ne pointe vers rien vers rien pour le moment
  - Politique de confidentialités - qui ne pointe vers rien vers rien pour le moment

## Configurer les premières routes statiques

Comment gérer les 3 routes statiques du site (page avec du contenu fixe seulement)

Objectif : créer une seule méthode qui va gérer les 3 pages statiques (A propos, Mentions, Politique) et les éventuelles futures pages statiques...

## Internationnalisez

Faire en sorte que notre site puisse être en français (fr) et en anglais (en)...

La langue devra être passé dans la route avec l'utilisation d'une locale.

Par défault la local devra être `fr` !

En fonction de la locale on ne chargera pas la même vue ;)

On rajoutera le choix de la langue dans la navBar => retour vers la page d'accueil avec la bonne locale...

Par défaut toutes les routes de notre Controller `FrontController` seront préfixées automatiquement par la locale

Les routes devront être internationalisée (exemple : /fr/produits  et /en/products )





