# Projet Ecommerce

## Installation

> Les étapes pour l'installation de ce projet à partir de 0 sur l'IDE 


**Ligne de commandes pour l'installation**
```bash
cd sites/
mkdir 3.ecommerce
cd 3.ecommerce/
# mise à jour de composer si nécessaire
composer self-update
# installation de la dernière version de la branche 5.4 de Symfony
composer create-project symfony/website-skeleton:"^5.4" ./
# installation du pack Apache pour le fonctionnement sur l'IDE
composer require symfony/apache-pack
# copie du fichier .env vers .env.local
cp .env .env.local
```

**Edition du .env.local**
```bash
# fichier .env.local
# on modifie la variable d'environnement DATABASE_URL : dirver Mysql, identifiant et nom de base préfixé !
DATABASE_URL="mysql://fabiens:MotDePasse@db.3wa.io:3306/fabiens_ecommerce?serverVersion=5.7"
```

**Création de la base de données**
```bash
php bin/console doctrine:database:create
```

**Installation du Webpack Encore**

**Ligne de commande d'intallation**
```bash
# installation des dépendances Composer
composer require symfony/webpack-encore-bundle
# installation des dépendances NPM
npm install #yarn install
# ajout du packet pour le framework CSS bootstrap
npm install bootstrap #yarn add bootstrap
# ajout des dépendances JS bootstrap
npm install @popperjs/core@^2.10.2 #yarn add @popperjs/core@^2.10.2
```

**Configuration de sass et import bootstrap**
```js
// fichier ./webpack.config.js

// modifiez cette ligne pour l'accès correcte au build de Webpack
// nous sommes sur l'IDE... donc il faut meettre un chemin d'accès correct
//.setPublicPath('/DOSSIERS_PROJET_DANS_LE_DOSSIER_SITES/public/build')
// par exemple si votre symfo est dans sites/symfony/3.ecommerce on met : 
.setPublicPath('/symfony/3.ecommerce/public/build')


// décommentez la ligne 
.enableSassLoader()
```

```js
// fichier ./asset/app.js

// any CSS you import will output into a single css file (app.css in this case)
//import './styles/app.css';
// on modifie la feuille de style en fichier SASS
import './styles/app.scss'

// import de boostrap JS
import 'bootstrap';

// start the Stimulus application
import './bootstrap';
```

```scss
// fichier ./asset/css/app.scss

// on modifie une varible d'environement de bootstrap pour tester
$body-bg: #000;

// on import les fichiers SASS de boostrap
@import '~bootstrap/scss/bootstrap';
```

```bash 
# installation des paquets SASS
npm install sass-loader@^12.0.0 sass --dev # yarn add sass-loader@^12.0.0 sass --dev

# pour compiler une fois en dev
npm run dev # yarn encore dev

# pour recompiler au moindre changement des assets (relancer cette commande si vous modifiez le fichier ./webpack.config.js)
npm run watch # yarn encore dev --watch
```

**Ajout d'icônes provenant d'icomoon avec le Webpack**

> - sur le site Icomoon.io créer votre librairie et exporter votre sélection
> - A partir de fichiers présents dans le ZIP téléchargé :
> - copier le fichier styles.css vers ./asset/css/icones.css
> - copier le dossier /fonts vers le dossier ./assets/fonts
> - modifier le fichier ./asset/css/icones.css pour faire le lien correct avec les fonts (liens relatif donc juste un ../ )
> - relancez la compilation des assets `npm run dev`
> - dans un fichier template twig, tester une icône avec `i class="icon-nomDeLicone"`




